#!/bin/sh

case $(hostname -f) in 

    "alitrdcr42.cern.ch" )
	echo "trdsite=sxl2"
	;;

    "alicetrd.phy.uct.ac.za" )
	echo "trdsite=uct"
	;;
    
    "vtrd01" )
	echo "trdsite=vagrant"
	;;

    *)
	echo "trdsite=UNKNOWN"
	;;

esac
	
