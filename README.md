# trd

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with trd](#setup)
    * [What trd affects](#what-trd-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with trd](#beginning-with-trd)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

Install and configure the low-level control software for the ALICE
TRD. This puppet module should be used at at CERN and all sites that
operate TRD readout chambers. 

The module is aimed at Scientific Linux / CentOS installations and
should be compatible with Puppet 3.x.

## Module Description

The module installs the proprietary software compenets that have been
developed for the TRD, or that this software depends on. This includes
pvss_mockup (with smmon and nginject), wingDB utilities, DIM...

## Setup

The module contains classes to configure multiple components that are
typically run at a site that operates TRD readout chambers. Include
the appropriate classes to configure them.

* DIM: client (trd::dim) and name server (trd::dim::dns)
* wingdb: client programs and configuration file

### Setup Requirements

This module does not set up the Oracle database that will host wingDB. 



## Usage

* DIM: include trd::dim to install the client programs and libraries,
  and configure the DIM_DNS_NODE environment variable.


## Reference

* trd::dim



## Limitations

The 

## Development

Since your module is awesome, other users will want to play with it. Let them
know what the ground rules for contributing are.

