# Class: sxl2::iseg2dim
#
# Set up iseg2dim for SM testing at SXL2
#
# Parameters:
#    (none)
#
# Actions:
#   - install iseg2dim
#   - install configuration file
#   - start iseg2dim service
#
# Requires:
#
# Sample Usage:
#
class trd::iseg2dim (
  $devices = 'can0',
  $aliases = {}
)
{

  package { [ 'iseg2dim', 'pcan', 'pcan-dkms' ]:
    ensure => 'latest',
  }

  File {
    ensure => 'file',
    owner => 'root',
    group => 'root',
    mode  => '0644',
  }    
  
  file {

    '/etc/iseg2dim.cfg':
      content => template('trd/iseg2dim.cfg.erb'),
      subscribe => Package['iseg2dim'];

    '/etc/init.d/canbus':
      mode   => '0755',
      content => template('trd/canbus-init.erb');
    
    '/etc/modprobe.d/pcan.conf':
      source => "puppet:///modules/trd/modprobe-pcan";

    '/etc/udev/rules.d/80-canbus.rules':
      source => "puppet:///modules/trd/udev-canbus";
    
  }

  service { 'iseg2dim':
    ensure     => 'running',
    subscribe  => File['/etc/iseg2dim.cfg'],
    require    => Service['canbus'],
  }
  
  service { 'canbus':
    ensure     => running,
    hasrestart => true,
    subscribe  => File['/etc/init.d/canbus', '/etc/modprobe.d/pcan.conf'],
  }
  
}



# Local Variables:
#   mode: puppet
#     puppet-indent-level: 4
#     indent-tabs-mode: nil
# End:
