
class trd::wingdb::config ( $wingdbname, $wingdbuser, $wingdbpass )
{

  include trd::base
  
  file { "/etc/trd/wingdb.conf":
    ensure   => file,
    content  => template("trd/wingdb.conf.erb"),
    mode     => '0644',
    owner    => 'root',
    group    => 'root',
    require  => File['/etc/trd']
  }

}
