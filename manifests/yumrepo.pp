# Class: trd::yumrepo
#
# short discription
#
# Parameters:
#   $first_parameter:
#       description
#
# Actions:
#   - 
#
# Requires:
#   - 
#
# Sample Usage:
#
class trd::yumrepo (
  String $base # URL of the Software Distribution Server
)
{

  $dist = $facts['os']['family'] ? {

    'RedHat' => $facts['os']['release']['major'] ? {

      '6'      =>  'slc6',
      default  =>  err ("RedHat version not supported")
    },

    default  =>  err ("distribution not supported")

  }
    
  yumrepo { 'trd':
    name      => 'trd',
    descr     => 'TRD Online Software', 
    target    => 'trd',
    baseurl   => "$base/$dist/",
    gpgcheck  => false,
    enabled   => true,
    protect   => false,
  }

    
}



# Local Variables:
#   mode: puppet
#     puppet-indent-level: 4
#     indent-tabs-mode: nil
# End:
