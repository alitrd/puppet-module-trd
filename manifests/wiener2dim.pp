# Class: trd::wiener2dim
#
# short discription
#
# Parameters:
#   $first_parameter:
#       description
#
# Actions:
#   - 
#
# Requires:
#   - 
#
# Sample Usage:
#
class trd::wiener2dim ( $channels )
{

  file { '/etc/trd/wiener2dim.cfg':
    content  => template('trd/wiener2dim.cfg.erb'),
    mode     => '0644',
    owner    => 'root',
    group    => 'root',
    notify   => Service['wiener2dim'],
  }

  service { 'wiener2dim':
    ensure => 'running',
    enable => true,
    hasstatus => true,
  }
  
  
}

# Local Variables:
#   mode: puppet
#     puppet-indent-level: 4
#     indent-tabs-mode: nil
# End:

