
# Class: trd::base
#
# Basic requirement to run (and compile) TRD software 
#
# Parameters:
#   (none)
#
# Actions:
#   - install various software packages
#
# Requires:
#   - 
#
# Sample Usage:
#
class trd::base
{

  require trd::yumrepo
  
  package {
    [

     # proprietary TRD programs 
     'pvss_mockup', 'iseghv', 'wingdb-utils', 'trapcc', 'trapasm',
     'libdim1', 'libdim-devel',
     
     # useful utilities
     'wget', 'curl', 'net-snmp',  'net-snmp-utils', 'nmap',
     'git', 'subversion', 'screen', 'tmux',
 
     # database client tools
      'oracle-instantclient', 'postgresql',

     # development tools
     'gcc', 'gcc-c++', 'make', 'redhat-rpm-config', 'rpm-build',
     'autoconf', 'automake', 'libtool',
     
     # development libraries
     'popt-devel', 'boost-devel', 'glib2-devel', 'poco-devel',
     'ncurses-devel', 'readline-devel', 'fmt-devel',
     'openssl-devel', 'net-snmp-devel',
     'oracle-instantclient-devel', 'postgresql-devel',

     # general system tools
     'redhat-lsb', 'mlocate',

     ]:
        
       ensure => 'installed',
       require => Class['trd::yumrepo'],
  }

  # make sure the config directory for the TRD exists
  file { '/etc/trd':
    ensure => 'directory',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0755',
  }

  # MIB file for Wiener PSUs
  file { '/usr/share/snmp/mibs/WIENER-CRATE-MIB.txt':
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/trd/WIENER-CRATE-MIB.txt',
    require => Package['net-snmp'],
  }
  
}


# Local Variables:
#   mode: puppet
#     puppet-indent-level: 4
#     indent-tabs-mode: nil
# End:
