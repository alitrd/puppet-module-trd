# Class: trd::netsrv
#
# Network services (DHCP, DNS) for a TRD network
#
# Parameters:
#   $first_parameter:
#       description
#
# Actions:
#   - 
#
# Requires:
#   - 
#
# Sample Usage:
#

class trd::netsrv (

  # network information
  $network = '10.180.0.0',
  $netmask = '255.255.0.0',
  $domain  = 'trd.local',
  $rdomain = '180.10.in-addr.arpa',

  $nameservers,
  
  # server information
  $srvname, # short name, without domain
  $srvip,
  $srvmac,
  $srviface,
  
  # other hosts on the network
  $hosts
)
{
  
  ## split the network base address into an array
  #$netarray = $net.split('\.')
  #
  ## create the network address
  #$network = [$netarray, Integer[0,3-$netarray.count()].map |$x| { "0" }
  #            ].flatten().join('.')
  #
  ## create the netmask
  #$netmask = [Integer[0,$netarray.count()-1].map |$x| { "255" }, 
  #            Integer[0,3-$netarray.count()].map |$x| { "0" }
  #            ].flatten().join('.')

  #notice ("Network: $network")
  #notice ("Netmask: $netmask")

  #notice($hosts.flatten())
  
  # Instantiate the network interface
  network::iface { $srviface:
    ensure     => 'up',
    bootproto  => 'none',
    ipaddress  => $srvip,
    netmask    => $netmask,
    macaddress => $srvmac,
    #gateway => $srvip,
    #mtu = '1500',
    #ethtool_opts = '',
    #peerdns = false,
    #dns1 = '',
    #dns2 = '',
    #domain => 'trd.local,'
  }

  class { 'network::dns':
    network => $network,
    #netmask => $netmask,

    srvip => $srvip,
    srvname => $srvname,
    #interfaces => [$srviface],
    nameservers => $nameservers,
    domain => $domain,
    rdomain => $rdomain,
    hosts => $hosts.flatten(),
    require => Network::Iface[$srviface],
  }
   
  # --------------------------------------------------------------
  # DHCP server
  class { 'network::dhcp':
    network => $network,
    netmask => $netmask,
    gateway => $srvip,
    interfaces => [$srviface],
    nameservers => [$srvip],
    dnsdomain  => [ $domain ],
    hosts => $hosts.flatten(),
    require => Network::Iface[$srviface],
    extraconf => {
      'wingdb' => {
        target  => "/etc/dhcp/dhcpd.conf",
        content => "use-host-decl-names on;\ninclude \"/etc/dhcp/dhcpd.wingdb\";\n",
        #content => "group {\nuse-host-decl-names on;\ninclude \"/etc/dhcp/dhcpd.wingdb\";\n}\n",
        order   => '80',
      },
    }
    
  }

  # ensure a configuration file for DHCP entries from the wingDB exists
  file { "/etc/dhcp/dhcpd.wingdb": ensure => 'file' }

  
  # --------------------------------------------------------------
  # IP forwarding and NAT
  sysctl { 'net.ipv4.ip_forward': value => '1' }

  
  
  
  
   
}



# Local Variables:
#   mode: puppet
#     puppet-indent-level: 4
#     indent-tabs-mode: nil
# End:
