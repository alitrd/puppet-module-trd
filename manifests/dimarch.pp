
# Class: trd::dimarch::server
#
# Install dimarchd - server to archive DIM data points into a DB 
#
# Parameters:
#   $first_parameter:
#       description
#
# Actions:
#   - 
#
# Requires:
#   - 
#
# Sample Usage:
#
class trd::dimarch::server
{

  package { 'dimarch': ensure => 'installed' }

  file { '/etc/trd/dimarch.xml':
    ensure  => 'file',
    owner   => 'root',
    group   => 'wheel',
    mode    => '0644',
    content => template('trd/dimarch.xml.erb'),
    notify  => Service['dimarchd'],
  }

  file { '/etc/trd/dimarch.conf':
    ensure  => 'absent',
  }

  
  service { 'dimarchd':
    ensure      => 'running',
    enable      => true,
    hasstatus   => true,
    hasrestart  => false,
  }
}



# Local Variables:
#   mode: puppet
#     puppet-indent-level: 4
#     indent-tabs-mode: nil
# End:
