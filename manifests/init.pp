# Class: trd
#
# basic configuration for ALICE TRD computers
#
# Parameters:
#   $sminfo:
#       information about TRD super-modules
#
# Actions:
#   - 
#
# Requires:
#   - 
#
# Sample Usage:
#
class trd ($sminfo = undef)
{
  include trd::base
}



# Local Variables:
#   mode: puppet
#     puppet-indent-level: 4
#     indent-tabs-mode: nil
# End:
