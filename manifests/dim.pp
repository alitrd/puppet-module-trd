# Class: trd::dim
#
# Install DIM client programs and set up environment variables
#
# Parameters:
#   $dnsnode:
#       Hostname of DIM name server, by default this is alitrddimdns,
#       which should be a DNS alias to the actual hostname. Use this
#       variable if no DNS alias is set up. 
#
# Actions:
#   - install dim-utils
#   - create bash profile snippet to set DIM_DNS_NODE
#
# Requires:
#
# Sample Usage:
#   include trd::dim
#   class { 'trd::dim': dnsnode => 'hostname.domainname' }
#
class trd::dim ($dnsnode = "alitrddimdns", $hostnode = undef)
{

  package {
    [ "dim-utils.${architecture}" ] :
      
      ensure => latest
  }

  file { "/etc/profile.d/dim.sh":
    ensure  => file,
    content => $hostnode ? {
      undef    => "export DIM_DNS_NODE=$dnsnode",
      default  => "export DIM_DNS_NODE=$dnsnode\nexport DIM_HOST_NODE=$hostnode"
    },
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
  }
}

class trd::dim::dns
{

  package { "dim-dns":
    ensure => 'installed',
  }

  service { "dim-dns":
    ensure    => 'running',
    enable    => true,
    hasstatus => true,
    status    => 'ps -eo comm | awk "BEGIN{R=3} /dim_dns/{R=0} END{exit R}"',
    require   => Package['dim-dns'],
  }
  
}

# Local Variables:
#   mode: puppet
#   puppet-indent-level: 2
#   indent-tabs-mode: nil
# End:
