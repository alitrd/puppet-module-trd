function trd::iseg_sm_aliases (Hash $sminfo) >> Hash {

  $sminfo

  .map |$sm,$cf| {

    $canbus = $cf['canbus'] ? {
      Undef => 'iseg2dim\.bus0',
      default => $cf['canbus']
    }

    $base = sprintf("HV.%s", $sm)
    
    $anodealias = $cf['anode'] ? {
      Undef => {},
      default => {
        "(${canbus}\\.mod${cf['anode']}.ch15\\..*)" => '\1',
      
        "${canbus}\\.mod(${cf['anode']}).ch(\\d+)\\.(.*)" =>
        "${base}_$[((\\1%2)*15+\\2)/6]_$[((\\1%2)*15+\\2)%6].ANODE.\\3",
      }
    }

    
    $driftalias = $cf['drift'] ? {
      Undef => {},
      default => {
        "(${canbus}\\.mod${cf['drift']}.ch15\\..*)" => '\1',
        
        "${canbus}\\.mod(${cf['drift']}).ch(\\d+)\\.(.*)" =>
        "${base}_$[((\\1%2)*15+\\2)/6]_$[((\\1%2)*15+\\2)%6].DRIFT.\\3",
      }
    }


    $data = $anodealias + $driftalias
  }

  .reduce |$memo,$value| { $memo + $value }

}

# Local Variables:
#   mode: puppet
#     puppet-indent-level: 4
#     indent-tabs-mode: nil
# End:
