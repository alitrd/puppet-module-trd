function trd::sm_dns_entries ($sm, $firstip) >> Array {
  $iptxt = $firstip.split('\.')[0,3].join('.')
  $ipidx = Integer($firstip.split('\.')[3])
  
  
  Integer[0,29].map() |$i| {
    [ { 'name' => sprintf("alidcsdcb%04d",$sm*30+$i),
      'ip' => sprintf("$iptxt.%d",$i+$ipidx) },
      { 'name' => sprintf("alitrddcb%02d%d%d",$sm, $i/6, $i%6),
      'cname' => sprintf("alidcsdcb%04d",30*$sm + $i) } ] }

}

